﻿Option Strict On

''' <Assignment>
''' <course> IS116 - Introduction to Computer Programming (VB)</course>
''' <Author> Brian Davis</Author>
''' <Project>Lab5A </Project>
''' <Date> May 19, 2014</Date>
''' </Assignment>
Public Class Form1


	''' <summary>
	'''  Only Allows numbers 0 through 9 to be entered, backspace for editing, and decimal.
	''' </summary>
	''' <param name="sender">Standard (IDE-supplied)</param>
	''' <param name="e">Key Press Event</param>
	''' <remarks>Prevents user at the textbox from entering invalid chars </remarks>
	Private Sub AssetCostTextBox_KeyPress(sender As Object, e As KeyPressEventArgs) Handles AssetCostTextBox.KeyPress

		If (e.KeyChar < "0" OrElse e.KeyChar > "9") AndAlso e.KeyChar <> ControlChars.Back AndAlso Not (e.KeyChar = ".") Then

			e.Handled = True  'Cancels the invalid key being pressed

		End If


	End Sub


	''' <summary>
	'''  Only Allows numbers 0 through 9 to be entered, backspace for editing, and decimal.
	''' </summary>
	''' <param name="sender">Standard (IDE-supplied)</param>
	''' <param name="e">A Key Press Event</param>
	''' <remarks>Prevents user at the textbox from entering invalid chars </remarks>
	Private Sub SalvageValueTextBox_KeyPress(sender As Object, e As KeyPressEventArgs) Handles SalvageValueTextBox.KeyPress


		If (e.KeyChar < "0" OrElse e.KeyChar > "9") AndAlso e.KeyChar <> ControlChars.Back AndAlso Not (e.KeyChar = ".") Then

			e.Handled = True  'Cancels the invalid key being pressed

		End If

	End Sub


	''' <summary>
	''' Performs User Input Data Validations.  It makes sure that the program is receiving correct information from the user
	''' and when incorrect information is entered from the user they are alerted when ever the UserDataValidation() method is called.
	''' This procedure is call by other procedures in order to run. 
	''' </summary>
	''' <remarks>Known Exceptions to watch out for http://msdn.microsoft.com/en-us/library/microsoft.visualbasic.financial.ddb.aspx</remarks>
	Public Sub UserDataValidation()

	Dim AssetCost, SalvageValue As Decimal
	Dim UsefulLife As Integer

		'Checks for any Empty TextBoxes 
		If (String.IsNullOrEmpty(Me.AssetCostTextBox.Text) Or String.IsNullOrEmpty(Me.SalvageValueTextBox.Text) Or String.IsNullOrEmpty(Convert.ToString(UsefulLifeListBox.SelectedItem))) Then

			Me.DepreciationScheduleReadOnlyTextBoxDisplay.Text = "Textboxes or Select List Can not be Empty"

			'Checks that a value is selected in the List Box
			ElseIf (String.IsNullOrEmpty(Convert.ToString(UsefulLifeListBox.SelectedItem))) Then

				Me.DepreciationScheduleReadOnlyTextBoxDisplay.Text = "Must Select Value for Useful Life"

			'If Both Textboxes are not Empty and there is a selected value in list box then load up their values into their own local  variables
			ElseIf (Not String.IsNullOrEmpty(Me.AssetCostTextBox.Text) AndAlso (Not String.IsNullOrEmpty(Me.SalvageValueTextBox.Text)) AndAlso (Not String.IsNullOrEmpty(Convert.ToString(UsefulLifeListBox.SelectedItem)))) Then

				Decimal.TryParse(Me.AssetCostTextBox.Text, AssetCost)
				Decimal.TryParse(Me.SalvageValueTextBox.Text, SalvageValue)
				Integer.TryParse(CStr(Me.UsefulLifeListBox.SelectedItem), UsefulLife)

		End If

		'Checks for AssetCost and SalvageValue making sure they are more than $0.00 and less than $999,999,999
		'Checking for known exceptions for the Financial.DDB Method  
		'Salvage < 0, Period <= 0
		'ToDo: Calculation Check  Period > Life Not an User Input Data validation, it is a Calculation Validation

		 If (AssetCost <= 0 Or SalvageValue < 0) Then

			Me.DepreciationScheduleReadOnlyTextBoxDisplay.Text = "Values Must be Greater than 0"

			 ElseIf (AssetCost > 999999999 Or SalvageValue > 999999999) Then

			  Me.DepreciationScheduleReadOnlyTextBoxDisplay.Text = "Value is Too Large!"

			 ElseIf (SalvageValue >= AssetCost) Then

				Me.SalvageValueTextBox.Text = "Must be less than Asset Cost"

		End If



	End Sub





	''' <summary>
	''' User moves their mouse over the Display Depreciation Schedule Button, when this is done the 	 
	''' UserDataValidation() procedure is called and errors from that procedure are displayed in the 
	''' Depreciation Schedule TextBox Display
	''' </summary>
	''' <param name="sender"> Standard (IDE-supplied)</param>
	''' <param name="e"> Mouse Hover</param>
	''' <remarks></remarks>
	Private Sub DisplayDepreciationScheduleButton_MouseHover(sender As Object, e As EventArgs) Handles DisplayDepreciationScheduleButton.MouseHover

	 UserDataValidation()

	End Sub




	''' <summary>
	''' 1. User Clicks on Display Depreciation Schedule Button 
	''' 2. Final Call to UserDataValidation() is made
	''' 3. If Textboxes are not empty or null and there is a value selected in the list box
	'''    The inputs from the user is loaded up into their own variables
	''' 4. For Next Loop is Run Calling the Financial.DDB method with current "Period"
	'''    For each loop adding the result to msg variable
	''' 5. Once looping is completed msg variable is assigned to the TextBoxDisplay to show the output  
	''' </summary>
	''' <param name="sender">Standard (IDE-supplied)</param>
	''' <param name="e">User Clicks on Display Depreciation Schedule Button </param>
	''' <remarks></remarks>
	Private Sub DisplayDepreciationScheduleButton_Click(sender As Object, e As EventArgs) Handles DisplayDepreciationScheduleButton.Click

		UserDataValidation()	 ''ToDo If UserDataValidation fails here need to stop program

		Dim Cost, Salvage, Life, Period, Periods As Double
		Dim Fmt As String = "###,##0.00"
		Dim msg As String = (vbTab & "Year" & vbTab & "Depreciation" & vbCrLf)

		'Loads Up Values from Textboxes and Select Box if they are not empty or Null into their own local variables. 
		If (Not String.IsNullOrEmpty(Me.AssetCostTextBox.Text) AndAlso (Not String.IsNullOrEmpty(Me.SalvageValueTextBox.Text)) AndAlso (Not String.IsNullOrEmpty(Convert.ToString(UsefulLifeListBox.SelectedItem)))) Then

				Double.TryParse(Me.AssetCostTextBox.Text, Cost)
				Double.TryParse(Me.SalvageValueTextBox.Text, Salvage)
				Double.TryParse(CStr(Me.UsefulLifeListBox.SelectedItem), Life)

				Periods = Life

		End If


		For Period = 1 To Periods

			msg &= vbTab & Period & vbTab & Format(DDB(Cost, Salvage, Life, Period), Fmt) & vbCrLf
		Next

		Me.DepreciationScheduleReadOnlyTextBoxDisplay.Text = msg


	End Sub


	''' <summary>
	'''  Exits the Program when the Exit Button is Clicked on
	''' </summary>
	''' <param name="sender">Standard (IDE-supplied)</param>
	''' <param name="e"> User Clicks Exit Button</param>
	''' <remarks></remarks>
	Private Sub ExitButton_Click(sender As Object, e As EventArgs) Handles ExitButton.Click

	Me.Close()

	End Sub



End Class











