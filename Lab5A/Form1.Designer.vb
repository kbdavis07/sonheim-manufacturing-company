﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
		Me.UsefulLifeLabel = New System.Windows.Forms.Label()
		Me.AssetCostLabel = New System.Windows.Forms.Label()
		Me.SalvageValueLabel = New System.Windows.Forms.Label()
		Me.AssetCostTextBox = New System.Windows.Forms.TextBox()
		Me.UsefulLifeListBox = New System.Windows.Forms.ListBox()
		Me.SalvageValueTextBox = New System.Windows.Forms.TextBox()
		Me.DepreciationScheduleLabel = New System.Windows.Forms.Label()
		Me.DepreciationScheduleReadOnlyTextBoxDisplay = New System.Windows.Forms.TextBox()
		Me.DisplayDepreciationScheduleButton = New System.Windows.Forms.Button()
		Me.ExitButton = New System.Windows.Forms.Button()
		Me.SuspendLayout()
		'
		'UsefulLifeLabel
		'
		Me.UsefulLifeLabel.AutoSize = True
		Me.UsefulLifeLabel.Location = New System.Drawing.Point(241, 21)
		Me.UsefulLifeLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.UsefulLifeLabel.Name = "UsefulLifeLabel"
		Me.UsefulLifeLabel.Size = New System.Drawing.Size(86, 21)
		Me.UsefulLifeLabel.TabIndex = 0
		Me.UsefulLifeLabel.Text = "&Useful Life:"
		'
		'AssetCostLabel
		'
		Me.AssetCostLabel.AutoSize = True
		Me.AssetCostLabel.Location = New System.Drawing.Point(20, 21)
		Me.AssetCostLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.AssetCostLabel.Name = "AssetCostLabel"
		Me.AssetCostLabel.Size = New System.Drawing.Size(85, 21)
		Me.AssetCostLabel.TabIndex = 1
		Me.AssetCostLabel.Text = "&Asset Cost:"
		'
		'SalvageValueLabel
		'
		Me.SalvageValueLabel.AutoSize = True
		Me.SalvageValueLabel.Location = New System.Drawing.Point(346, 21)
		Me.SalvageValueLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.SalvageValueLabel.Name = "SalvageValueLabel"
		Me.SalvageValueLabel.Size = New System.Drawing.Size(110, 21)
		Me.SalvageValueLabel.TabIndex = 2
		Me.SalvageValueLabel.Text = "&Salvage Value:"
		'
		'AssetCostTextBox
		'
		Me.AssetCostTextBox.Location = New System.Drawing.Point(24, 50)
		Me.AssetCostTextBox.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
		Me.AssetCostTextBox.Name = "AssetCostTextBox"
		Me.AssetCostTextBox.Size = New System.Drawing.Size(185, 29)
		Me.AssetCostTextBox.TabIndex = 3
		'
		'UsefulLifeListBox
		'
		Me.UsefulLifeListBox.FormattingEnabled = True
		Me.UsefulLifeListBox.ItemHeight = 21
		Me.UsefulLifeListBox.Items.AddRange(New Object() {"3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20"})
		Me.UsefulLifeListBox.Location = New System.Drawing.Point(245, 50)
		Me.UsefulLifeListBox.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
		Me.UsefulLifeListBox.Name = "UsefulLifeListBox"
		Me.UsefulLifeListBox.Size = New System.Drawing.Size(69, 88)
		Me.UsefulLifeListBox.TabIndex = 4
		'
		'SalvageValueTextBox
		'
		Me.SalvageValueTextBox.Location = New System.Drawing.Point(350, 47)
		Me.SalvageValueTextBox.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
		Me.SalvageValueTextBox.Name = "SalvageValueTextBox"
		Me.SalvageValueTextBox.Size = New System.Drawing.Size(212, 29)
		Me.SalvageValueTextBox.TabIndex = 5
		'
		'DepreciationScheduleLabel
		'
		Me.DepreciationScheduleLabel.AutoSize = True
		Me.DepreciationScheduleLabel.Location = New System.Drawing.Point(20, 140)
		Me.DepreciationScheduleLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.DepreciationScheduleLabel.Name = "DepreciationScheduleLabel"
		Me.DepreciationScheduleLabel.Size = New System.Drawing.Size(168, 21)
		Me.DepreciationScheduleLabel.TabIndex = 6
		Me.DepreciationScheduleLabel.Text = "Depreciation Schedule:"
		'
		'DepreciationScheduleReadOnlyTextBoxDisplay
		'
		Me.DepreciationScheduleReadOnlyTextBoxDisplay.Location = New System.Drawing.Point(24, 166)
		Me.DepreciationScheduleReadOnlyTextBoxDisplay.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
		Me.DepreciationScheduleReadOnlyTextBoxDisplay.Multiline = True
		Me.DepreciationScheduleReadOnlyTextBoxDisplay.Name = "DepreciationScheduleReadOnlyTextBoxDisplay"
		Me.DepreciationScheduleReadOnlyTextBoxDisplay.ReadOnly = True
		Me.DepreciationScheduleReadOnlyTextBoxDisplay.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
		Me.DepreciationScheduleReadOnlyTextBoxDisplay.Size = New System.Drawing.Size(474, 180)
		Me.DepreciationScheduleReadOnlyTextBoxDisplay.TabIndex = 7
		'
		'DisplayDepreciationScheduleButton
		'
		Me.DisplayDepreciationScheduleButton.BackColor = System.Drawing.Color.SeaGreen
		Me.DisplayDepreciationScheduleButton.ForeColor = System.Drawing.SystemColors.ControlLightLight
		Me.DisplayDepreciationScheduleButton.Location = New System.Drawing.Point(24, 356)
		Me.DisplayDepreciationScheduleButton.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
		Me.DisplayDepreciationScheduleButton.Name = "DisplayDepreciationScheduleButton"
		Me.DisplayDepreciationScheduleButton.Size = New System.Drawing.Size(235, 37)
		Me.DisplayDepreciationScheduleButton.TabIndex = 8
		Me.DisplayDepreciationScheduleButton.Text = "&Display Depreciation Schedule"
		Me.DisplayDepreciationScheduleButton.UseVisualStyleBackColor = False
		'
		'ExitButton
		'
		Me.ExitButton.BackColor = System.Drawing.Color.Firebrick
		Me.ExitButton.ForeColor = System.Drawing.SystemColors.ControlLightLight
		Me.ExitButton.Location = New System.Drawing.Point(386, 356)
		Me.ExitButton.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
		Me.ExitButton.Name = "ExitButton"
		Me.ExitButton.Size = New System.Drawing.Size(112, 37)
		Me.ExitButton.TabIndex = 9
		Me.ExitButton.Text = "E&xit"
		Me.ExitButton.UseVisualStyleBackColor = False
		'
		'Form1
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 21.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.ClientSize = New System.Drawing.Size(622, 409)
		Me.Controls.Add(Me.ExitButton)
		Me.Controls.Add(Me.DisplayDepreciationScheduleButton)
		Me.Controls.Add(Me.DepreciationScheduleReadOnlyTextBoxDisplay)
		Me.Controls.Add(Me.DepreciationScheduleLabel)
		Me.Controls.Add(Me.SalvageValueTextBox)
		Me.Controls.Add(Me.UsefulLifeListBox)
		Me.Controls.Add(Me.AssetCostTextBox)
		Me.Controls.Add(Me.SalvageValueLabel)
		Me.Controls.Add(Me.AssetCostLabel)
		Me.Controls.Add(Me.UsefulLifeLabel)
		Me.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
		Me.Name = "Form1"
		Me.Text = "Sonheim Manufacturing Company"
		Me.ResumeLayout(False)
		Me.PerformLayout()

End Sub
	Friend WithEvents UsefulLifeLabel As System.Windows.Forms.Label
 Friend WithEvents AssetCostLabel As System.Windows.Forms.Label
 Friend WithEvents SalvageValueLabel As System.Windows.Forms.Label
 Friend WithEvents AssetCostTextBox As System.Windows.Forms.TextBox
 Friend WithEvents UsefulLifeListBox As System.Windows.Forms.ListBox
 Friend WithEvents SalvageValueTextBox As System.Windows.Forms.TextBox
 Friend WithEvents DepreciationScheduleLabel As System.Windows.Forms.Label
 Friend WithEvents DepreciationScheduleReadOnlyTextBoxDisplay As System.Windows.Forms.TextBox
 Friend WithEvents DisplayDepreciationScheduleButton As System.Windows.Forms.Button
 Friend WithEvents ExitButton As System.Windows.Forms.Button

End Class
